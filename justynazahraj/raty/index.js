
var count = document.getElementById('btn_count');
count.addEventListener('click', countCredit);

function countCredit(){
    // table_result.innerHTML = '';

    var amount = document.getElementById('loan_amount').value;
    amount = amount * 100;
    var months = document.getElementById('loan_months').value;
    var interest = document.getElementById('loan_interest').value;

    var monthly_installment = amount / months; //stała co miesiąc

    var amount_sum = 0;
    var odsetki_sum = 0;
    var kwota_raty_sum = 0;
    var tbody = document.querySelector('tbody');
    tbody.innerHTML = '';

    for(var i=1; i<=months; i++){
    
        var odsetki = amount*interest/100/months;
        amount = amount -  monthly_installment;
        var kwota_raty = monthly_installment + odsetki;

        var tr = document.createElement('tr');

        tr.innerHTML = `<td class="">${i}</td>
        <td>${(monthly_installment/100).toFixed(2)}</td>
        <td>${(odsetki/100).toFixed(2)}</td>
        <td>${(amount/100).toFixed(2)}</td>
        <td>${(kwota_raty/100).toFixed(2)}</td>`;


        // var show = `<tr><td class="">${i}</td>
        // <td>${(monthly_installment/100).toFixed(2)}</td>
        // <td>${(odsetki/100).toFixed(2)}</td>
        // <td>${(amount/100).toFixed(2)}</td>
        // <td>${(kwota_raty/100).toFixed(2)}</td></tr>`

        // table_result.innerHTML += show;

        tbody.appendChild(tr);

        odsetki_sum = odsetki_sum + odsetki;
        kwota_raty_sum = kwota_raty_sum + kwota_raty;
    }
    
    
    // console.log((odsetki_sum/100).toFixed(2));
    // console.log((kwota_raty_sum/100).toFixed(2));

    amount_sum = monthly_installment/100*months;
    // console.log(amount_sum.toFixed(2));

    var tr_sum = document.createElement('tr');

    tr_sum.innerHTML = `<td class="" style="font-weight: bold;">Suma: </td>
    <td style="font-weight: bold;">${(amount_sum).toFixed(2)}</td>
    <td style="font-weight: bold;">${(odsetki_sum/100).toFixed(2)}</td>
    <td style="font-weight: bold;"></td>
    <td style="font-weight: bold;">${(kwota_raty_sum/100).toFixed(2)}</td>`;


    // var show = `<tr><td class="">${i}</td>
    // <td>${(monthly_installment/100).toFixed(2)}</td>
    // <td>${(odsetki/100).toFixed(2)}</td>
    // <td>${(amount/100).toFixed(2)}</td>
    // <td>${(kwota_raty/100).toFixed(2)}</td></tr>`

    // table_result.innerHTML += show;

    tbody.appendChild(tr_sum);

    // var show_sum = `<tr><td class="" style="font-weight: bold;">Suma: </td>
    // <td style="font-weight: bold;">${(amount_sum).toFixed(2)}</td>
    // <td style="font-weight: bold;">${(odsetki_sum/100).toFixed(2)}</td>
    // <td style="font-weight: bold;"></td>
    // <td style="font-weight: bold;">${(kwota_raty_sum/100).toFixed(2)}</td></tr>`

    // table_result.innerHTML += show_sum;
}

