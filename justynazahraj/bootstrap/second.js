
var products = [
    {
        id: 1, 
        name: "Kapusta", 
        price: 299, 
        desc: "Świeża kapusta do kiszenia",  
        promotion: true,
        discount: 0.1, 
        image:"https://swiezenatalerze.pl/pol_pl_KAPUSTA-BIALA-SWIEZA-BIO-POLSKA-10066_1.jpg", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 2, 
        name: "Burak", 
        price: 319, 
        desc: "Idealne buraki do barszczu",  
        promotion: true, 
        image:"https://sklep-nasiona.pl/#", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 3,
        name: "Gruszka", 
        price: 422, 
        desc: "Soczyste gruszki",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        date_added: new Date, 
        category: "owoce"
    },
    {
        id: 4, 
        name: "Seler", 
        price: 299, 
        desc: "Świeży seler",  
        promotion: true,
        discount: 0.1, 
        image:"", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 5, 
        name: "Sałata", 
        price: 319, 
        desc: "Idealna na kanapki",  
        promotion: true, 
        image:"", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 6,
        name: "Czereśnia", 
        price: 422, 
        desc: "Soczyste czereśnie",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        date_added: new Date, 
        category: "owoce"
    },
];


var cartItems = [];
var cartTotal = 0;
var tax=23;
var shop_discount = 0.5;
var discount_of_product = 0;

// i=0;

var promoted = true;

productInfo(products, 'refresh');


var btn_refresh = document.getElementById('refresh_button');
btn_refresh.onclick = function(){productInfo(products, 'refresh')};

var btn_promoted = document.getElementById('promoted_button');
btn_promoted.onclick = function(){productInfo(products, 'promoted')};


/**
 * Function that count discount of all products in list
 * @param { Array<Object> } 
 * 
 */
function productInfo(products, button_type) {
    products_list.innerHTML = '';

    if(button_type === 'refresh'){
        for (i = 0; i < products.length; i++) {
            if (products[i].promotion === true) {

                if (products[i].discount != undefined) {
                    discount_of_product = products[i].discount;
                }
                else {
                    discount_of_product = shop_discount;
                }
            }

            var final_price = countPrice();
            var final_price_tax = countPriceTax();
            var final_price_tax = countPriceTax().toFixed(2);
            var item = renderToHtml(products, final_price_tax);
        
            products_list.innerHTML += item;
        }
    }
    else{
        for (i = 0; i < products.length; i++) {
            if (products[i].promotion === true) {

                if (products[i].discount != undefined) {
                    discount_of_product = products[i].discount;
                }
                else {
                    discount_of_product = shop_discount;
                }
                var final_price = countPrice();
                var final_price_tax = countPriceTax();
                var final_price_tax = countPriceTax().toFixed(2);
                var item = renderToHtml(products, final_price_tax);
            
                products_list.innerHTML += item;
            }
        }
    };

    function countPriceTax() {
        return (final_price + final_price * tax / 100) / 100;
    }

    function countPrice() {
        return products[i].price - (products[i].price * discount_of_product / 100);
    }
}



function renderToHtml(products, final_price_tax) {
    var product_list = `<div class="list-group-item d-flex justify-content-between" id="product_${products[i].id}">
                            <input type="text" id="product_value_${products[i].id}" value="${final_price_tax}" hidden>
                            <div class="d-flex flex-column">
                                <h3>${products[i].name}</h3>
                                <p>${products[i].desc}</p>
                            </div>
                            <div class="d-flex-column">
                                <div class="text-center my-2 fw-bold">${final_price_tax} USD</div>
                                <button class="btn btn-info my-2" onclick="addToCart('${products[i].id}')">Add cart</button>
                            </div>
                        </div>`;
    return product_list;
}


function addToCart(product_id){

    console.log("Cart: ", cartItems);

    var index = cartItems.findIndex(item => item.product_id == product_id);
    
    if (index == -1) {
        var product = getProductById(product_id);
 
        cartItems.push({product_id:product.id, product, amount:1, subtotal: product.price});
        console.log("append: ", cartItems);
        return cartItems;

      } else {
        
        console.log("else: ", cartItems);

        return cartItems.map(function (item) {
            if (item.product_id !== parseInt(product_id)) {return item } 
                var amount = item.amount += 1;
                var subtotal = item.subtotal = item.product.price * amount;
                return {
                    ...item, amount, subtotal
                }
          });  
      }
    //   Wywołać funkcję wyświetlającą elementy koszyka (updateCartView)
}


function updateCartView(newItems) {
    var listEl = document.querySelector('.js-cart-items')
  
    newItems.forEach(function (item, index) {
      var el = listEl.querySelector(`[data-item-id="${item.product_id}"]`)
      // Add New
      if (!el) {
        var el = document.createElement('div')
        el.innerHTML = `<li data-item-id="${item.product_id}" class="list-group-item js-product d-flex justify-content-between lh-sm">
            <div>
              <h6 class="my-0 js-product-name"></h6>
              <small class="text-muted">Brief description</small>
            </div>
            <span class="text-muted js-item-subtotal">${item.subtotal}</span>
            <span class="close">&times;</span>
          </li>`
        el = el.firstChild
        listEl.appendChild(el)
      }
      // Update existing
      el.querySelector('.js-product-name').innerText = item.product.name
      el.querySelector('.js-item-subtotal').innerText = item.subtotal
    })
    // Remove rest
    cartItems.forEach(function (item) {
      if (newItems.indexOf(item) == -1) {
        listEl.querySelector(`[data-item-id="${item.product_id}"]`).remove()
      }
    })
    cartItems = newItems
  }



function getProductById(product_id) {
    return products.find(function (prod) {
      return prod.id == product_id
    })
  }



// function renderCart() {
//     var itemElem = document.querySelector('.js-cart-items')
//     itemElem.innerHTML = ''
//     for (let line of cartItems) {
//       itemElem.innerHTML += renderCartItem(line)
//     }
//   }



// KOSZYK

// var shopping_cart = `<div class="list-group-item d-flex shopping_cart_style" id="price_${products[i].id}">
// <div class="d-flex-column">
//     <button class="btn btn-info my-2" onclick="deleteProduct('product_${products[i].id}', 'product_value_${products[i].id}', 'price_${products[i].id}')">X</button>
// </div>
// </div>`;



function renderToHtmlTotal(total) {
    total = total.toFixed(2);

    return `<div class="list-group-item d-flex justify-content-end">
                <div class="d-flex flex-row">
                    <h3>Suma: </h3>
                    <h3 class="ms-5">${total} USD</h3>
                    <input value="${total}" id="total_in_shopping_cart" hidden>
                </div>
            </div>`;
}



var category="warzywa";

checkCategoryDEscription(category);

function checkCategoryDEscription(category) {
    switch (category) {
        case "warzywa": description = "W"; break;
        case "owoce": description =  "O";
        default: "inne";

    }
}

function deleteProduct(productID, productValueID, priceID){
    var price_to_remove = parseFloat(document.getElementById(productValueID).value);
    var total_removed = parseFloat(document.getElementById("total_in_shopping_cart").value);

    var removed_total_final = total_removed - price_to_remove;

    removed_total_final.toFixed(2);
    total_price_section.innerHTML = '';
    var final_price = renderToHtmlTotal(removed_total_final);
    total_price_section.innerHTML += final_price;

    var el = document.getElementById(productID);
    el.remove(); 
    var el_shop = document.getElementById(priceID);
    el_shop.remove();
}


