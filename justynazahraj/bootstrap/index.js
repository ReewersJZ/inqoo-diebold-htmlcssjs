
var products = [
    {
        name: "Kapusta", 
        price: 299, 
        desc: "Świeża kapusta do kiszenia",  
        promotion: true,
        discount: 0.1, 
        image:"https://swiezenatalerze.pl/pol_pl_KAPUSTA-BIALA-SWIEZA-BIO-POLSKA-10066_1.jpg", 
        id: 1, 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        name: "Burak", 
        price: 319, 
        desc: "Idealne buraki do barszczu",  
        promotion: true, 
        image:"https://sklep-nasiona.pl/#", 
        id: 2, 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        name: "Gruszka", 
        price: 422, 
        desc: "Soczyste gruszki",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        id: 3,
        date_added: new Date, 
        category: "owoce"
    },
    {
        name: "Seler", 
        price: 299, 
        desc: "Świeży seler",  
        promotion: true,
        discount: 0.1, 
        image:"", 
        id: 1, 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        name: "Sałata", 
        price: 319, 
        desc: "Idealna na kanapki",  
        promotion: true, 
        image:"", 
        id: 2, 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        name: "Czereśnia", 
        price: 422, 
        desc: "Soczyste czereśnie",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        id: 3,
        date_added: new Date, 
        category: "owoce"
    },
];

var tax=23;
var shop_discount = 0.5;

var discount_of_product = 0;
i=0;

var promoted = true;


var btn_refresh = document.getElementById('refresh_button');
btn_refresh.onclick = function(){productInfo(products)};

var btn_promoted = document.getElementById('promoted_button');
btn_promoted.onclick = function(){productInfoPromoted(products)};


/**
 * Function that count discount of all products in list
 * @param { Array<Object> } 
 * 
 */
function productInfo(products) {
    var total = 0;
    products_list.innerHTML = '';
    shopping_cart.innerHTML = '';
    total_price_section.innerHTML = '';

    for (i = 0; i < products.length; i++) {
        if (products[i].promotion === true) {

            if (products[i].discount != undefined) {
                discount_of_product = products[i].discount;
            }
            else {
                discount_of_product = shop_discount;
            }
        }
        else {
            // console.log("no discount");
        }
        var final_price = countPrice();
        var final_price_tax = countPriceTax();
        var final_price_tax = countPriceTax().toFixed(2);

        var productID = i;
        
        var item = renderToHtml(products, final_price_tax, productID);
       
        products_list.innerHTML += item[0];
        shopping_cart.innerHTML += item[1];

        final_price_tax = parseFloat(final_price_tax); 
        total += final_price_tax;
    }

    var total_price = renderToHtmlTotal(total);
    total_price_section.innerHTML += total_price;

    function countPriceTax() {
        return (final_price + final_price * tax / 100) / 100;
    }

    function countPrice() {
        return products[i].price - (products[i].price * discount_of_product / 100);
    }
}

/**
 * Function that filter only promoted products
 * @param { Array<Object> } 
 * 
 */
 function productInfoPromoted(products) {
    var total = 0;
    products_list.innerHTML = '';
    shopping_cart.innerHTML = '';
    total_price_section.innerHTML = '';

    for (i = 0; i < products.length; i++) {
        if (products[i].promotion === true) {

            if (products[i].discount != undefined) {
                discount_of_product = products[i].discount;
            }
            else {
                discount_of_product = shop_discount;
            }
            var final_price = countPrice();
            var final_price_tax = countPriceTax();
            var final_price_tax = countPriceTax().toFixed(2);

            var productID = i;
            
            var item = renderToHtml(products, final_price_tax, productID);
        
            products_list.innerHTML += item[0];
            shopping_cart.innerHTML += item[1];

            final_price_tax = parseFloat(final_price_tax); 
            total += final_price_tax;
        }
        else {
            // console.log("no discount");
        }
        
    }

    var total_price = renderToHtmlTotal(total);
    total_price_section.innerHTML += total_price;

    function countPriceTax() {
        return (final_price + final_price * tax / 100) / 100;
    }

    function countPrice() {
        return products[i].price - (products[i].price * discount_of_product / 100);
    }
}


var category="warzywa";

checkCategoryDEscription(category);


function renderToHtml(products, final_price_tax, productID) {
    var product_list = `<div class="list-group-item d-flex justify-content-between" id="product_${productID}">
                            <input type="text" id="product_value_${productID}" value="${final_price_tax}" hidden>
                            <div class="d-flex flex-column">
                                <h3>${products[i].name}</h3>
                                <p>${products[i].desc}</p>
                            </div>
                            <div class="d-flex-column">
                                <div class="text-center my-2 fw-bold">${final_price_tax} USD</div>
                                <button class="btn btn-info my-2">Add cart</button>
                            </div>
                        </div>`;
    var shopping_cart = `<div class="list-group-item d-flex shopping_cart_style" id="price_${productID}">
                            <div class="d-flex-column">
                                <button class="btn btn-info my-2" onclick="deleteProduct('product_${productID}', 'product_value_${productID}', 'price_${productID}')">X</button>
                            </div>
                        </div>`;
    
    var item_to_html = [product_list, shopping_cart];

    return item_to_html;
}

function renderToHtmlTotal(total) {
    total = total.toFixed(2);

    return `<div class="list-group-item d-flex justify-content-end">
                <div class="d-flex flex-row">
                    <h3>Suma: </h3>
                    <h3 class="ms-5">${total} USD</h3>
                    <input value="${total}" id="total_in_shopping_cart" hidden>
                </div>
            </div>`;
}

function checkCategoryDEscription(category) {
    switch (category) {
        case "warzywa": description = "W"; break;
        case "owoce": description =  "O";
        default: "inne";

    }
}

function deleteProduct(productID, productValueID, priceID){
    var price_to_remove = parseFloat(document.getElementById(productValueID).value);
    var total_removed = parseFloat(document.getElementById("total_in_shopping_cart").value);

    var removed_total_final = total_removed - price_to_remove;

    removed_total_final.toFixed(2);
    total_price_section.innerHTML = '';
    var final_price = renderToHtmlTotal(removed_total_final);
    total_price_section.innerHTML += final_price;

    var el = document.getElementById(productID);
    el.remove(); 
    var el_shop = document.getElementById(priceID);
    el_shop.remove();
}


