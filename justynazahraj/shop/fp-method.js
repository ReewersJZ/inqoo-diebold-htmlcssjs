
var products = [
    {
        id: 1, 
        name: "Kapusta", 
        price: 299, 
        desc: "Świeża kapusta do kiszenia",  
        promotion: true,
        discount: 10, 
        image:"https://swiezenatalerze.pl/pol_pl_KAPUSTA-BIALA-SWIEZA-BIO-POLSKA-10066_1.jpg", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 2, 
        name: "Burak", 
        price: 319, 
        desc: "Idealne buraki do barszczu",  
        promotion: true, 
        image:"https://sklep-nasiona.pl/#", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 3,
        name: "Gruszka", 
        price: 422, 
        desc: "Soczyste gruszki",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        date_added: new Date, 
        category: "owoce"
    },
    {
        id: 4, 
        name: "Seler", 
        price: 299, 
        desc: "Świeży seler",  
        promotion: true,
        discount: 0.1, 
        image:"", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 5, 
        name: "Sałata", 
        price: 319, 
        desc: "Idealna na kanapki",  
        promotion: true, 
        image:"", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 6,
        name: "Czereśnia", 
        price: 422, 
        desc: "Soczyste czereśnie",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        date_added: new Date, 
        category: "owoce"
    },
];


var cartItems = [];
var cartTotal = 0;
var tax= 8;
var shop_discount = 50;
var discount_of_product = 0;
var numOfProducts = 0;
var promoted = true;

productInfo(products, 'refresh');

var btn_refresh = document.getElementById('refresh_button');
btn_refresh.onclick = function(){productInfo(products, 'refresh')};

var btn_promoted = document.getElementById('promoted_button');
btn_promoted.onclick = function(){productInfo(products, 'promoted')};


/**
 * Function that count discount of all products in list
 * @param { Array<Object> } 
 * 
 */
function productInfo(products, button_type) {
    if(button_type === 'refresh'){
        products_list.innerHTML = '';
        for (i = 0; i < products.length; i++) {

            if (products[i].discount != undefined && products[i].promotion === true) {
                discount_of_product = products[i].discount;
            }
            else if (products[i].discount != undefined && products[i].promotion === false){
                discount_of_product = 0;
            }
            else {
                discount_of_product = shop_discount;
            }

            var final_price = countPrice();
            var final_price_tax = countPriceTax();
            var final_price_tax = countPriceTax().toFixed(2);
            var item = renderToHtml(products, final_price_tax);
        
            products_list.innerHTML += item;
        }
    }
    else if(button_type === "promoted"){
        products_list.innerHTML = '';
        for (i = 0; i < products.length; i++) {
            if (products[i].promotion === true) {

                if (products[i].discount != undefined) {
                    discount_of_product = products[i].discount;
                }
                else {
                    discount_of_product = shop_discount;
                }
                var final_price = countPrice();
                var final_price_tax = countPriceTax();
                var final_price_tax = countPriceTax().toFixed(2);
                var item = renderToHtml(products, final_price_tax);
            
                products_list.innerHTML += item;
            }
        }
    }
    else if(button_type === "adToCart"){
            if (products.discount != undefined && products.promotion === true) {
                discount_of_product = products.discount;
            }
            else if (products.discount != undefined && products.promotion === false){
                discount_of_product = 0;
            }
            else {
                discount_of_product = shop_discount;
            }

            var final_price = countPriceAddToCart();
            var final_price_tax = countPriceTax();
            var final_price_tax = countPriceTax().toFixed(2);
            return final_price_tax;
    };

    function countPriceTax() {
        return (final_price + final_price * tax / 100) / 100;
    }

    function countPrice() {
        return products[i].price - (products[i].price * discount_of_product / 100);
    }

    function countPriceAddToCart() {
        return products.price - (products.price * discount_of_product / 100);
    }
}



function renderToHtml(products, final_price_tax) {
    var product_list = `<div class="list-group-item d-flex justify-content-between" id="product_${products[i].id}">
                            <input type="text" id="product_value_${products[i].id}" value="${final_price_tax}" hidden>
                            <div class="d-flex flex-column">
                                <h3>${products[i].name}</h3>
                                <p>${products[i].desc}</p>
                            </div>
                            <div class="d-flex-column">
                                <div class="text-center my-2 fw-bold">${final_price_tax} USD</div>
                                <button class="btn btn-info my-2" onclick="addToCart('${products[i].id}')">Add cart</button>
                            </div>
                        </div>`;
    return product_list;
}


function addToCart(product_id){
    var index = cartItems.findIndex(item => item.product_id == product_id);
    if (index == -1) {
        var product = getProductById(product_id);

        var final_product = productInfo(product, "adToCart");

        cartItems.push({product_id:product.id, product, amount:1, subtotal: final_product});
        numOfProducts = cartItems.reduce(function( sum, el){ return sum + el.amount }, 0);
        cartTotal = cartItems.reduce(function( sum, el){ return sum + parseFloat(el.subtotal) }, 0).toFixed(2);;
        updateCartView(cartItems);
        updateCartTotalView(numOfProducts, cartTotal)
        return cartItems;

      } else {
        return cartItems.map(function (item) {
            if (item.product_id !== parseInt(product_id)) {return item } 
                var amount = item.amount += 1;
                var final_price = updateCartPrice(item);
                var subtotal = item.subtotal = final_price * amount;
                numOfProducts = cartItems.reduce(function( sum, el){ return sum + el.amount }, 0);
                cartTotal = cartItems.reduce(function( sum, el){ return sum + parseFloat(el.subtotal) }, 0).toFixed(2);
                updateCartView(cartItems);
                updateCartTotalView(numOfProducts, cartTotal)
                return {
                    ...item, amount, subtotal
                }
          });    
      }
}



function updateCartPrice(item){
    if (item.product.discount != undefined && item.product.promotion === true) {
        discount_of_product = item.product.discount;
    }
    else if (item.product.discount != undefined && item.product.promotion === false){
        discount_of_product = 0;
    }
    else {
        discount_of_product = shop_discount;
    }
    
    var final_price = countPrice();
    var final_price_tax = countPriceTax();

    function countPriceTax() {
        return (final_price + final_price * tax / 100) / 100;
    }

    function countPrice() {
        return item.product.price - (item.product.price * discount_of_product / 100);
    }

    return final_price_tax.toFixed(2);
}

function getProductById(product_id) {
    return products.find(function (prod) {
      return prod.id == product_id
    })
  }


function updateCartView(newItems) {
    var listEl = document.querySelector('.js-cart-items')
  
    newItems.forEach(function (item) {

      var el = listEl.querySelector(`[data-item-id="${item.product_id}"]`)
      // Add New
      if (!el) {
        var el = document.createElement('div')
        el.innerHTML = `<li data-item-id="${item.product_id}" class="list-group-item js-product d-flex justify-content-between lh-sm">
            <div>
              <h6 class="my-0 js-product-name"></h6>
              
            </div>
            <span class="text-muted js-item-subtotal">${item.subtotal}</span>
            <span class="close" id="button_delete_${item.product_id}" onclick="deleteProductFromCart(${item.product_id})">&times;</span>
          </li>`
        el = el.firstChild
        listEl.appendChild(el)
      }
      // Update existing
      el.querySelector('.js-product-name').innerText = item.product.name + " x " + item.amount;
      el.querySelector('.js-item-subtotal').innerText = item.subtotal + " USD";
    })
  }

  function updateCartTotalView(numOfProducts, cartTotal) {
    var elSum =  document.getElementById('numberOfItems');
    elSum.innerHTML = `<hr><li  class="list-group-item js-product d-flex justify-content-between lh-sm">
                            <div>
                                <h6 class="my-0 js-product-name">Ilość produktów w koszyku: </h6>
                            </div>
                            <span class="">${numOfProducts} szt.</span>
                        </li>`;

    var elTotal =  document.getElementById('CartTotal');
    elTotal.innerHTML = `<li  class="list-group-item js-product d-flex justify-content-between lh-sm">
                        <div>
                            <h6 class="my-0 js-product-name">Kwota do zapłaty: </h6>
                        </div>
                        <span class="">${cartTotal} USD</span>
                    </li>`;
  }


  function deleteProductFromCart(productID){
    var listEl = document.querySelector('.js-cart-items')
    listEl.querySelector(`[data-item-id="${productID}"]`).remove();
    var filtered = cartItems.filter(element => element.product_id != productID); //The same as function "delElementFromCart" below
    cartItems = filtered;

    numOfProducts = cartItems.reduce(function( sum, el){ return sum + el.amount }, 0);
    cartTotal = cartItems.reduce(function( sum, el){ return sum + parseFloat(el.subtotal) }, 0).toFixed(2);

    updateCartTotalView(numOfProducts, cartTotal);

    // function delElementFromCart(element) {
    //     return (element.product_id != productID);
    //   }
  }




