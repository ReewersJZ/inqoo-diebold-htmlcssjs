var products = [
    {
        id: 1, 
        name: "Kapusta", 
        price: 299, 
        desc: "Świeża kapusta do kiszenia",  
        promotion: true,
        discount: 10, 
        image:"https://swiezenatalerze.pl/pol_pl_KAPUSTA-BIALA-SWIEZA-BIO-POLSKA-10066_1.jpg", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 2, 
        name: "Burak", 
        price: 319, 
        desc: "Idealne buraki do barszczu",  
        promotion: true, 
        image:"https://sklep-nasiona.pl/#", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 3,
        name: "Gruszka", 
        price: 422, 
        desc: "Soczyste gruszki",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        date_added: new Date, 
        category: "owoce"
    },
    {
        id: 4, 
        name: "Seler", 
        price: 299, 
        desc: "Świeży seler",  
        promotion: true,
        discount: 0.1, 
        image:"", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 5, 
        name: "Sałata", 
        price: 319, 
        desc: "Idealna na kanapki",  
        promotion: true, 
        image:"", 
        date_added: new Date, 
        category: "warzywa"
    },
    {
        id: 6,
        name: "Czereśnia", 
        price: 422, 
        desc: "Soczyste czereśnie",  
        promotion: false,
        discount: 0.25, 
        image:"#", 
        date_added: new Date, 
        category: "owoce"
    },
];

class ShopView {
    
    constructor(selector, tax, shop_discount, promoted) {
      this.el = document.querySelector(selector);
      this.tax = tax;
      this.shop_discount = shop_discount;
      this.promoted = promoted;
    }

    buildShop(products) {
        products_list.innerHTML = '';
        const div = document.createElement('div')
        products.forEach(element => {
            const final_price_tax = this.countFinalPrice(element, false);
            div.innerHTML = this.renderItem(element, final_price_tax);
            const li = div.firstElementChild
            this.el.appendChild(li);
        });
      }

     countFinalPrice(element, type){
        let discount_of_product = 0;

        if(type == true){
            if (element.discount != undefined) {
                discount_of_product = element.discount;
            }
            else {
                discount_of_product = this.shop_discount;
            }
        }
        else{
            if (element.discount != undefined && element.promotion === true) {
                discount_of_product = element.discount;
            }
            else if (element.discount != undefined && element.promotion === false){
                discount_of_product = 0;
            }
            else {
                discount_of_product = this.shop_discount;
            }
        }
        
        let final_price = element.price - (element.price * discount_of_product / 100);
        let final_price_tax = (final_price + final_price * this.tax / 100) / 100;
        return final_price_tax.toFixed(2);
    }
    
      renderItem(item, final_price_tax) {

        return `<div class="list-group-item d-flex justify-content-between" id="product_${item.id}">
                    <input type="text" id="product_value_${item.id}" value="${final_price_tax}" hidden>
                    <div class="d-flex flex-column">
                        <h3>${item.name}</h3>
                        <p>${item.desc}</p>
                    </div>
                    <div class="d-flex-column">
                        <div class="text-center my-2 fw-bold">${final_price_tax} USD</div>
                        <button class="btn btn-info my-2" onclick="addToCart('${item.id}')">Add cart</button>
                    </div>
                </div>`;
      }
}

class ShopPromotedView extends ShopView{
      buildShop(products) {
        products_list.innerHTML = '';
        const div = document.createElement('div');
        var products_promoted = products.filter(function (promoted){ return promoted.discount});
        products_promoted.forEach(element => {
            const final_price_tax = this.countFinalPrice(element, true);
            div.innerHTML = this.renderItem(element, final_price_tax);
            const li = div.firstElementChild
            this.el.appendChild(li);
        });
      }
}


class BasketView {
    constructor(selector, cartItems, numOfProduct, cartTotal) {
      this.el = document.querySelector(selector);
      this.cartItems = cartItems;
      this.numOfProducts = numOfProduct;
      this.cartTotal = cartTotal;
    }
  
    /**
     * 
     * @param {CartItem} item 
     */

    addToCartItem(product_id){
        var index = this.cartItems.findIndex(item => item.product_id == product_id);
        if (index == -1) {
            var product = getProductById(product_id);

            function getProductById(product_id) {
                return products.find(function (prod) {
                  return prod.id == product_id
                })
              }

            let final_price_tax = shop.countFinalPrice(product, false);

            this.cartItems.push({product_id:product.id, product, amount:1, subtotal: final_price_tax});
            this.numOfProducts = this.cartItems.reduce(function( sum, el){ return sum + el.amount }, 0);
            this.cartTotal = this.cartItems.reduce(function( sum, el){ return sum + parseFloat(el.subtotal) }, 0).toFixed(2);;
            basketView.updateCartView(this.cartItems);
            basketView.updateCartTotalView(this.numOfProducts, this.cartTotal)
            return this.cartItems;
    
          } else {
            this.cartItems = this.cartItems.map(function (item) {
                if (item.product_id !== parseInt(product_id)) {return item } 
                    var amount = item.amount += 1;
                    var final_price = shop.countFinalPrice(item.product, true);
                    var subtotal = item.subtotal = final_price * amount;
                    return {
                        ...item, amount, subtotal
                    }
              });  

            this.numOfProducts = this.cartItems.reduce(function( sum, el){ return sum + el.amount }, 0);
            this.cartTotal = this.cartItems.reduce(function( sum, el){ return sum + parseFloat(el.subtotal) }, 0).toFixed(2);
            basketView.updateCartView(this.cartItems);
            basketView.updateCartTotalView(this.numOfProducts, this.cartTotal)
          }
    }

    deleteFromCart(productID){

        var listEl = document.querySelector('.js-cart-items')
        listEl.querySelector(`[data-item-id="${productID}"]`).remove();
        var filtered = this.cartItems.filter(element => element.product_id != productID);
        this.cartItems = filtered;
    
        this.numOfProducts = this.cartItems.reduce(function( sum, el){ return sum + el.amount }, 0);
        this.cartTotal = this.cartItems.reduce(function( sum, el){ return sum + parseFloat(el.subtotal) }, 0).toFixed(2);
    
        basketView.updateCartTotalView(this.numOfProducts, this.cartTotal);
    
      }
    
    updateCartTotalView(numOfProducts, cartTotal) {
        var elSum =  document.getElementById('numberOfItems');
        elSum.innerHTML = `<hr>
                            <li  class="list-group-item js-product d-flex justify-content-between lh-sm">
                                <div>
                                    <h6 class="my-0 js-product-name">Ilość produktów w koszyku: </h6>
                                </div>
                                <span class="">${numOfProducts} szt.</span>
                            </li>`;
    
        var elTotal =  document.getElementById('CartTotal');
        elTotal.innerHTML = `<li  class="list-group-item js-product d-flex justify-content-between lh-sm">
                                <div>
                                    <h6 class="my-0 js-product-name">Kwota do zapłaty: </h6>
                                </div>
                                <span class="">${cartTotal} USD</span>
                            </li>`;
      }

    updateCartView(newItems) {
        var listEl = document.querySelector('.js-cart-items')
      
        newItems.forEach(function (item) {
    
          var el = listEl.querySelector(`[data-item-id="${item.product_id}"]`)

          if (!el) {
            var el = document.createElement('div')
            el.innerHTML = `<li data-item-id="${item.product_id}" class="list-group-item js-product d-flex justify-content-between lh-sm">
                                <div>
                                    <h6 class="my-0 js-product-name"></h6>
                                </div>
                                <span class="text-muted js-item-subtotal">${item.subtotal}</span>
                                <span class="close" id="button_delete_${item.product_id}" onclick="deleteProductFromCart(${item.product_id})">&times;</span>
                            </li>`
            el = el.firstChild
            listEl.appendChild(el)
          }
          el.querySelector('.js-product-name').innerText = item.product.name + " x " + item.amount;
          el.querySelector('.js-item-subtotal').innerText = item.subtotal + " USD";
        })
      }
}


function showProducts(object){
    object.buildShop(products);
}

var shop = new ShopView('.list-group', 23, 25, true);
shop.buildShop(products);

var btn_refresh = document.getElementById('refresh_button');
var shopAll = new ShopView('.list-group', 23, 25, true);
btn_refresh.addEventListener('click', function(){showProducts(shopAll)});

var btn_promoted = document.getElementById('promoted_button');
var shopPromoted = new ShopPromotedView('.list-group', 23, 25, true);
btn_promoted.addEventListener('click', function(){showProducts(shopPromoted)});

 
var basketView = new BasketView('.js-cart-items', [], 0, 0);

function addToCart(product_id){
    basketView.addToCartItem(product_id);
}

function deleteProductFromCart(productID){
    basketView.deleteFromCart(productID);
  }