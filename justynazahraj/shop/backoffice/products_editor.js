class ProductsEditorView{

    constructor(selector){
        this.el = document.querySelector(selector);
        this.form = this.el.querySelector('form');
        this.form.addEventListener('submit', event => {
            event.preventDefault() // Stop form from reloading the page!
      
            // console.log(this.getData())
            const data = this.getData()
            fetch('http://localhost:3000/products/'+ data.id, {
                method: 'PUT',
                headers: {'content-Type': 'application/json'},
                body: JSON.stringify(data)
            })
            .then(res =>res.json())
            .then(()=>{
                products.myCallback();
            })
            })
    }

    /**
     * @param {Product} 
     * 
     */
     _data

    setData(data){
        if(data.length === 0){
            this.form.elements["name"].value = '';
            this.form.elements["price"].value = '';
        }
        else{
                this._data = data;
                this.form.elements["name"].value = this._data[0].name;
                this.form.elements["price"].value = this._data[0].price;
        }

    }

    getData(){
       this._data[0].name = this.form.elements["name"].value 
        this._data[0].price= this.form.elements["price"].value
        return this._data[0];
    }

    clearForm(){
        this.form.elements["name"].value = '';
        this.form.elements["price"].value = '';
    }

    

}

var productEditor = new ProductsEditorView('#product-editor');

