

class Product{

    constructor(id, name, price){
        this.id = id;
        this.name = name;
        this.price = price;
    }
}


class ProductsView{
    constructor(products, selector){
        this.products = products;
        this.filtered = products;
        this.el = document.querySelector(selector);
        this.clicked;
    }

    show(){
        this.el.innerHTML = '';
        const div = document.createElement('div')
        this.products.forEach(element => {
            
            div.innerHTML = this.renderItem(element);
            const li = div.firstElementChild;
            this.el.appendChild(li);
        });
    }

    renderItem(item){
            return `<div class="list-group-item d-flex justify-content-between" id="product_${item.id}" data-user-id=${item.id}>
                        <input type="text" id="product_value_${item.id}" value="" hidden>
                        <div class="d-flex flex-column">
                            <h3 >${item.name}</h3>
                            <p>test</p>
                        </div>
                        <div class="d-flex-column">
                            <div class="text-center my-2 fw-bold" >${item.price} USD</div>
                        </div>
                    </div>`;
    }

    push(product){
        this.products.push(product);
        return products;
    }

    clearList(){
        this.products.splice(0);
        products.show();
    }

    productRemove(itemID){
        function ifindProd(item) {
            return item.id != itemID;
          }
          
          let filtered = this.products.filter(ifindProd)
          this.products = filtered;
          return products.show();
    }

    filterByName(name){
        function findProdByName(item) {
            // return item.name === name;
            return item.name .includes (name);
          }
          
        //   let filtered = new ProductsView([], '.products_list');
        //   let filtered = this.products.filter(findProdByName);
        //   filtered = new ProductsView(filtered, '.products_list');
        //   console.log(filtered);

        this.filtered = this.products.filter(findProdByName);

        //   this.products = filtered;
          return this.show();
    }

    filterByID(id){
        function findProdByID(item) {
            id = parseInt(id);
            return item.id === id;
          }
          
        let itemToShow = this.products.filter(findProdByID);
          return itemToShow;
    }
    

    addActive(event){
        var elementItem = document.querySelectorAll(".list-group-item")
        // elementItem.forEach((element) => element.setAttribute("style", "background-color: white; color: black;"));
        elementItem.forEach((element) => element.classList.remove("active"));
        // var myevent = event.path[0];

        var myevent = event.target.closest("[data-user-id]");
        myevent.classList.add("active");

        // console.log("my event", myevent);

        let myeventID = myevent.getAttribute("data-user-id");

        // console.log("clicked", myeventID);

        if (this.clicked === myeventID){
            myevent.classList.remove("active");
            this.clicked = null;
            productEditor.clearForm();
        }
        else{
            this.clicked = myeventID;
        }

        appEvents.comunicateEvent('changed_edited_product', this.clicked);
        products.myCallbackByID(myeventID);

        // console.log("clicked", this.clicked);

        // myevent.setAttribute("style", "background-color: blue; color: white;");

        // myevent.classList.add("active");
        // myevent = myevent.getAttribute("data-user-id");
        // console.log(myevent);
       };

       
           
        myCallback(){
            this.loadJSON('http://localhost:3000/products').then((resp) => {this.products = resp; this.show()});
            
        }

        myCallbackByID(id){
            this.loadJSON('http://localhost:3000/products' + "/" + id).then(console.log("True"));
        }
       

        loadJSON(url){
            return new Promise((myCallback)=>{                          //to jest .then z metody myyCallback
                var xhr = new XMLHttpRequest();                         //to jest .then z metody myyCallback
                xhr.open('GET', url);                                   //to jest .then z metody myyCallback
                xhr.addEventListener('loadend', event =>{               //to jest .then z metody myyCallback
                    const data = JSON.parse(event.target.responseText); //to jest .then z metody myyCallback
                    myCallback(data);                                   //to jest .then z metody myyCallback
                });                                                     //to jest .then z metody myyCallback
                xhr.send();                                             //to jest .then z metody myyCallback
            })
        };

  
    
}

class App{
    constructor(event){
        this.event = event;
    }

    comunicateEvent(event, clicked){
        if(event === 'changed_edited_product'){
            productEditor.setData(products.filterByID(clicked));
            products.myCallback();
        }
        else{'changed_other'}
    }


}



// loadJSON('database.json', resp => {return(resp)});

// var aaa = loadJSON('database.json', resp => {return(resp)});
// console.log("aaa", aaa);

// var database = products.filterJSON();
// console.log("database", database);


var appEvents = new App('');

var products = new ProductsView([], '.products_list');
products.myCallback();






// products.products.push(products.filterJSON());
// console.log("filtredJSON", products);


// var product1 = new Product(1, "Sok", 299);
// var product2 = new Product(2, "Kefir", 25);
// var product3 = new Product(3, "Maślanka",235);

// products.push(product1);
// products.push(product2);
// products.push(product3);



var clickedElement = document.querySelector(".list-group");
clickedElement.addEventListener("click", products.addActive);

// var butonSave = document.querySelector(".btn-success");
// butonSave.addEventListener("click", products.butonSave());





