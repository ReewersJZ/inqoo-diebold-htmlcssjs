class Cart {

  constructor() {
    /** @type CartItem[] */
    this._cartItems = []
    this._cartTotal = 0

  }

  reset() {
    this._cartItems = []
    this._cartTotal = 0
  }

  addProduct(product_id) {
    /** @type Product */
    const product = this.products.getById(product_id)

    const item = this._cartItems.find(item => item.product_id === product_id)

    if (item) {
      item.amount++
      this._calculateItem(item)
    } else {
      const amount = 1
      this._cartItems.push({
        // product: product,
        product_id,
        product,
        amount,
        subtotal: product.price * amount,
      })
    }
    // cartTotal = this._cartItems.reduce((sum, item) => sum + item.subtotal, 0)
    this._cartTotal += product.price
  }

  getTotal() {
    // return cartTotal
    return this._cartItems.reduce((sum, item) => sum + item.subtotal, 0)
  }

  _calculateItem(item) {
    item.subtotal = item.product.price * item.amount
  }

  removeProduct(product_id) {
    const item = this._cartItems.find(item => item.product_id === product_id)

    if (item) {
      item.amount--
      this._calculateItem(item)

      if (item.amount <= 0) {
        this._cartItems = this._cartItems.filter(i => i.product_id !== item.product_id)
      }
      // cartTotal = this._cartItems.reduce((sum, item) => sum + item.subtotal, 0)
      this._cartTotal -= item.product.price
    }
  }

  getItems() {
    return this._cartItems
  }
}

class Products {

  /**
   * Collection of Products 
   * @param {Product[]} products 
   */
  constructor(products) {
    this._products = products
  }

  /**
   * Find product By Id
   * @param {string} product_id 
   * @returns Product
   */
  getById(product_id) {
    return this._products.find(product => product.id === product_id)
  }
}

var cart = new Cart()
cart.products = new Products(window.products)

// window.basket = new Cart()

// makeCart(window.cart = {})
// makeCart(window.basket = {})


/**  @type Product */
    // var prod = productsGetById('123')
    // prod.name.toUpperCase()

/**
 * @typedef Product
 * @property {string} id
 * @property {string} name
 * @property {number} price Nett price in cents
 */

/**
 * @typedef CartItem
 * @property {string} product_id
 * @property {Product} product
 * @property {number} amount
 * @property {number} subtotal
 */
