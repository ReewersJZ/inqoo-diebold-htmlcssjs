class ProductFormEvent extends Event {
  constructor(type, data) { super(type); this.data = data }
}
class ProductEditorView extends EventTarget {

  constructor(selector) {
    super() // EventTarget.constructor
    this.el = document.querySelector(selector)
    this.form = this.el.querySelector('form')

    this.form.addEventListener('submit', event => {
      event.preventDefault() // Stop form from reloading the page!

      // console.log(this.getData())
      this.dispatchEvent(new ProductFormEvent('product_saved', this.getData()))
    })
  }

  /** 
   * Product data
   * @type Product
   * @protected
   */
  _data = {}

  /**
   * @param {Product} data 
   */
  setData(data) {
    this._data = data
    this.form.elements['name'].value = this._data['name']
    this.form.elements['price'].value = (this._data['price_nett'] / 100).toFixed(2)
  }

  /**
   * @returns Product
   */
  getData() {
    const f = new FormData(this.form)
    const data = Object.fromEntries(f.entries())
    console.log(data)

    this._data['name'] = this.form.elements['name'].value
    this._data['price_nett'] = parseFloat(this.form.elements['price'].value) * 100

    return this._data
  }


}

